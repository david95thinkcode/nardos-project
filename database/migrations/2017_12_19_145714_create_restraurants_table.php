<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestraurantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('raison_sociale')->unique();
            $table->string('tel')->unique()->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('adresse')->nullable();
            $table->timestamps();
            // POur l'instant c'est tout
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('restaurants');
    }
}
