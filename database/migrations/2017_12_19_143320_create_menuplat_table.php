<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuplatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuplat', function (Blueprint $table) {
            $table->increments('id_menuplat');

            $table->unsignedInteger('id_plat')->nullable();
            $table->foreign('id_plat')
                    ->references('id_plat')
                    ->on('plats')
                    ->onDelete('cascade');

            $table->unsignedInteger('id_menu')->nullable();
            $table->foreign('id_menu')
                    ->references('id_menu')
                    ->on('menus')
                    ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menuplat');
    }
}
