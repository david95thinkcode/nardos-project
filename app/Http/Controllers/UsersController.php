<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\table\restaurant;

use App\Models\table\utilisateurs;

class UsersController extends Controller
{
	public function postInfos(Request $request)

        {
			// contrainte au niveau du formulaire
      if (($request->input('nom')==null||
	  $request->input('tel')==null||
	  $request->input('email')==null||
	  $request->input('adr')==null||
	  $request->input('desc')==null )) 
	  {   
         echo "<h4>vous navez pas renseigner un champ verifiez et reesayer!!!!!!<h4>";
         return view('restaurant/creerrestaurant');
      }  
			 // Enregistrement dans la base de donnees   
	 $var= new restaurant;
	 $var -> nom=$request->input('nom');
	 $var -> tel=$request->input('tel');
	 $var -> email=$request->input('email');
	 $var -> adresse= $request->input('adr');
	 $var -> description=$request->input('desc');
	 $var ->  save();
     return redirect('/listrestaurant');
      }

    public function formulaire_creation_restaurant()
		{
			return view('restaurant.creerrestaurant');
		}

public function ajouteRepas()
		{
			return view('restaurant.ajouteRepas');
		}

	
	 public function listrestaurant()
	{
		// selection de la table et mis dans $results
		$results = \DB::table('restaurant')->get();;
	    return view('restaurant.listrestaurant')->with('results', $results);               
    }

     public function modiferSaisie($id)
	{
	    $var = new restaurant;
		$var = restaurant::find($id);
	    return view('restaurant.modifirestaurant')->with('var', $var);               
    }
	
	   public function supprimerrestaurant($id, Request $request)
	{
		$var = new restaurant;
		$var = restaurant::find($id);
	    $var->delete();
	    return redirect()->back();
    }
	
	   public function rafraichirSaisie(Request $request)
		
		{
//			  //$bar = restaurant::find($id);// renvoi un objet restaurant	
 $objrestaurant = new restaurant;
			   $id = $request->input('id');
               $objrestaurant = restaurant::find($id);// renvoi   dobjet restaurant
			   $objrestaurant->nom = $request->input('nom');
			   $objrestaurant->tel = $request->input('tel');
			   $objrestaurant->email = $request->input('email');
			   $objrestaurant->adresse = $request->input('adr');
			   $objrestaurant->description = $request->input('desc');	
			   $objrestaurant->save();
			
			return redirect('/listrestaurant');
		}
		  

		// methode inscription
		public function inscription()	 	 
		{	 
			return view('restaurant.inscription');
					
		}

	// methode connexion
		public function connexion()
		{
			return view('restaurant.connexion');
		}


	public function acceuil()
	{
		return view('restaurant.template');
	}

	//methode de creation d'utilisateurss
		public function creerutilisateur()
		{
			$restaurant = new Restaurant();
             $restau = \DB::table('restaurant')->get();
	   	return view('restaurant.utilisateur')->with('restau', $restau);

        }
			 
    public function Enregistrer(Request $request)
	{
			 	// contrainte au niveau du formulaire
      if (($request->input('pseudo')==null|| 
	       $request->input('pass')==null||
	       $request->input('email')==null )) 
  	  {   
         echo "<h5>vous navez pas renseigner un champ verifiez et reesayer!!!!!!<h5>";
         return view('restaurant.utilisateur');
      }  
			 // Enregistrement dans la base de donnees   
			$vari = new utilisateurs;
			$vari -> id=$request->input('id');
			$vari -> pseudo=$request->input('pseudo');
			$vari -> pass= $request->input('pass');
			$vari -> email=$request->input('email');
			$vari -> save();
		   return redirect('/listeutilisateur');
       
	}

   public function listeutilisateur()
   {
	    $result = \DB::table('utilisateurs')->get();;
	    return view('restaurant.listeutilisateur')->with('result', $result);    
   }

		}