<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tableRestau\plat;
use App\Models\tableRestau\menu;
use App\Models\tableRestau\menuplat;
use App\Http\Requests;

class MenuController extends Controller
{
    
    public function Ajouter() {
        $plats = Plat::all();
        //return view('restau/creermenu', compact('plats'));
        return view('layouts.menus.ajouter', compact('plats'));
    }

    public function Store(Request $request){

        $menu = new menu;
        $selectedPlat = $request->get('plats'); // Récupération des plats sélectionnés

        $menu -> libmenu = $request->input('libmenu');
        $menu ->  save();
        
        // Enregistrement dans la table MenuPlat de chaque plat sélectionné
        foreach ($selectedPlat as $key => $value ) {
            $mp = new MenuPlat;
            $mp->id_plat = $value;
            $mp->id_menu = $menu->id_menu;
            $mp->save();
        }

        return redirect()->route('menu.ajouter');
    }

    public function List() {
        $menus = Menu::all();
        // TODO : à completer lorsque la vue des listes sera créée
    }
}
