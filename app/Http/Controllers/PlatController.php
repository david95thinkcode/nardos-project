<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests;
use App\Models\tableRestau\plat;

class PlatController extends Controller
{

    public function Ajouter() {
        // return view ('restau/ajoutplat'); ancienne vue
        return view('plats/ajouter'); // nouvelle vue dynamique
    }


    //remplace la methode 
    public function store(Request $request) {
        $plat = new Plat;

        $plat -> nom = $request->input('nomPlat');
        $plat -> description = $request->input('description');
        
        $photo=$request->file('photo');
        $upload='upload/photo';
        $filename=$photo->getClientOriginalName();
        $plat -> image = $filename;
        Storage::put('upload/photo/'.$filename,file_get_contents($request->file('photo')->getRealPath()));
       
        $plat->save();
        
        return view ('restau/ajoutplat');
    }

    public function List() {
        $plats = Plat::all();
        
        //return view()->compact('plats');
    }
}
