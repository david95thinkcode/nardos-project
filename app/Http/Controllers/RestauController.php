<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests;

use App\Models\tableRestau\users;
use App\Models\tableRestau\plat;
use App\Models\tableRestau\restau;
use App\Models\tableRestau\client;
use App\Models\tableRestau\menu;
use App\Models\tableRestau\menuplat;

class RestauController extends Controller
{

public function afficheinscription()
{
  return view ('restau.essainscription');
}


 public function afficheconnexion()
 {
   return view ('restau.essaiconnexion');
 }



    public function acceuil()
    {
        return view('restau.acceuil');
        //return view('david');
    }
    
    // Methode image
    public function addImage(Request $request)
    {
     // contrainte au niveau du formulaire
      if (($request->input('nom')==null||
	        $request->input('image')==null||
	        $request->input('description')==null))
	    {   
         echo "vous navez pas renseigner un champ verifiez et reesayer!";
         return view('restau/ajoutplat');
      }  
			 // Enregistrement dans la base de donnees   
	     $var= new plat;
	     $var -> nomPlat=$request->input('nom');
	     $var -> imagePlat=$request->input('image');
	     $var -> description=$request->input('description');
	     $var ->  save();
           return redirect('/listrestaurant');
    }
    // Fin methode image

// calendrier
public function events()
{
  return view('restau/calendrier');
}

// Methode Ajout Client
public function AddClient(Request $request)
{
  // contrainte au niveau du formulaire
 if (($request->input('nom')==null||
	    $request->input('telephone')==null||
	    $request->input('email')==null||
	    $request->input('adresse')==null||
  	  $request->input('date')==null )) 
	  {   
       echo "vous navez pas renseigner un champ verifiez et reesayer!!!!!!";
       return view('restau/enregistrerclient');
    }  
			 // Enregistrement dans la base de donnees   
	   $var = new client;
	   $var -> nom=$request->input('nom');
	   $var -> telephone=$request->input('telephone');
	   $var -> email=$request->input('email');
	   $var -> adresse= $request->input('adresse');
	   $var -> date=$request->input('date');
	   $var ->  save();

      echo " client enregistrer avec succes";
     return redirect('/listeClient');
}

public function editClient($id)
{
      $var = new client;
	  	$var = client::find($id);
	    return view('restau.modifierclient')->with('var', $var);    
}

public function rafrachirclient(Request $request)
{
         $objetclient = new client;
			   $id = $request->input('id_client');
         $objetclient = client::find($id);// renvoi   dobjet restaurant
			   $objetclient->nom = $request->input('nom');
			   $objetclient->telephone = $request->input('telephone');
			   $objetclient->email = $request->input('email');
			   $objetclient->adresse = $request->input('adresse');
			   $objetclient->description = $request->input('date');	
			   $objetclient->save();
}

public function deleteclient($id, Request $request)
{
      $var = new client;
   		$var = client::find($id);
	    $var->delete();
	    return redirect()->back();
}
public function checklistClient()
{
  //selection de la table et mis dans $results
		$results = \DB::table('client')->get();;
    return view ('restau/listeclient')->with('results', $results);;
}
// fin Methode Ajout client

public function savecommande()
{
  return view ('restau/commande');

}

public function form()
{
   return view('restau/enregistrerclient');
}

}
