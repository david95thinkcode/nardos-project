<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Input;

use App\Models\Message;

class MessagerieController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

      /*--------------------------------------service------------------------------------*/

        //liste des mesages
    public function getMessagesList() {
      $objectSearch = Message::all();
      return $objectSearch;
    }


        //cree un message
    public function createMessage() {
              //recup les champs fournis
      $inputArray = Input::get();

                //verifie les champs fournis
      if (!( isset($inputArray['TITRE']) && isset($inputArray['CONTENU']) ))  { //controle d existence
           return array("status" => "error",
                "error" => "Vous ne pouvez pas accéder à cette fonctionnalité");
        }

      $contenu = $inputArray['CONTENU'];
      $titre = $inputArray['TITRE'];

      //cree le message
      $obj = new Message;
      $obj->TITRE = $titre;
      $obj->CONTENU = $contenu;
      $obj->STATUT = false;
      $obj->save();

      return $obj;
    }

    //met a jour un message
    public function updateMessage() {
             //recup les champs fournis
      $inputArray = Input::get();

            //verifie les champs fournis
      if (!( isset($inputArray['ID']) && isset($inputArray['TITRE'])  && isset($inputArray['CONTENU']) ))  { //controle d existence
           return array("status" => "error",
                "error" => "Vous ne pouvez pas accéder à cette fonctionnalité");
        }

      $id = $inputArray['ID'];
      $titre = $inputArray['TITRE'];
      $contenu = $inputArray['CONTENU'];

        //verifier que l'id est bien un id entier
        if (!preg_match( '/^\d+$/', $id)) {
                   return array("status" => "error",
                "error" => "L'identifiant du message n'est pas valide");
        }

      //recupere le message
      $objSearch = Message::where("ID", "=", "$id")->get();
      if ($objSearch->isEmpty()) {
            return array("status" => "error",
                "error" => "Aucun message enregistré avec cet identifiant");
        }
      $obj = $objSearch->first();

      //met a jour
        $obj->TITRE = $titre;
        $obj->CONTENU = $contenu;
        $obj->save();

      return $obj;
    }

    //supprime un message
    public function deleteMessage($id) {

      //verifier ke lid est valide
        if (!preg_match( '/^\d+$/', $id)) {
                   return array("status" => "error",
                "error" => "L'identifiant du message n'est pas valide");
        }

        //recupere l objet
      $objSearch = Message::where("ID", "=", "$id")->get();
      if ($objSearch->isEmpty()) {
            return array("status" => "error",
                "error" => "Aucun message enregistré avec cet identifiant");
        }
      $obj = $objSearch->first();

      //supprimer
        $obj->delete();

      //retour
      return array("status" => "success",
                "info" => "L'objet a été supprimé");
    }


        //recupere un nouveau message
    public function  getNewMessage(){

        $objectMsgSearch = Message::where("STATUT", "=", false)->get();
        $countMsg = $objectMsgSearch->count();

        //si aucun nest au statut false, on les remet ts a true et on reprend
        if ($countMsg == 0){
            //on les mets ts a jour
            $objectMsgAll = Message::all();
            foreach ($objectMsgAll as $msg){
                $msg->STATUT =  false;
                $msg->save();
            }
            //on reessaie
            $objectMsgSearch = Message::where("STATUT", "=", false)->get();
            $countMsg = $objectMsgSearch->count();
        }

        $choix = 1;
        try{
            $choix = mt_rand(1, $countMsg);
        }catch(Exception $e){
            $choix = 1;
        }

        $index = 1;
        $selectedMessage = null;
        foreach ($objectMsgSearch as $msg){
            if ($index == $choix) {
                $msg->STATUT =  true;
                $msg->save();
                return $msg;
            }
            $index++;
        }
        return $selectedMessage;
    }//fin recu message

}
