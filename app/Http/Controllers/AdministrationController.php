<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Input;

use App\Models\Message;
use App\Models\Administrateur;
use App\Models\Pays;
use App\Models\Utilisateur;

class AdministrationController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth');
    }

    //login
    public function login($user, $password) {

        $objSearch = Administrateur::where("USER_NAME", "LIKE", "$user")
        ->where("PASSWORD", "LIKE", "$password")->get();
        if ($objSearch->isEmpty()) {
            return array("status" => "error",
                "error" => "Vos paramètres de connexion ne sont pas valides");
        }
        $obj = $objSearch->first();
        $result = array("status" => true);
        return $result;
    }

           /*--------------------------------------------------------------------------*/

        //liste des pays
    public function getPaysList() {
      $objSearch = Pays::all();
      return $objSearch;
    }


        //cree une profil
    public function createProfile() {
              //recup les champs fournis
      $inputArray = Input::get();
                //verifie les champs fournis
      if (!( isset($inputArray['code']) &&   isset($inputArray['phone'])))  { //controle d existence
           return array("status" => "error",
                "error" => "Vous ne pouvez pas accéder à cette fonctionnalité");
        }

      $code = $inputArray['code'];
      $phone = $inputArray['phone'];

        //sassurer que le pays existe bien
        $objPaysSearch = Pays::where("CODE", "LIKE", "$code")->get();
        if ($objPaysSearch->isEmpty()) {
            return array("status" => "error",
                "error" => "Cet indicatif de pays n'est pas valide");
        }
        $obj = $objPaysSearch->first();

        //sassurer que le numero nest pas deja enregistre
        $objUserSearch = Utilisateur::where("TELEPHONE", "LIKE", "$phone")->get();
        if (!$objUserSearch->isEmpty()) {
            return array("status" => "error",
                "error" => "Ce numéro est déjà utilisé");
        }
        $objUser = $objUserSearch->first();

        //cree le user
        $user = new Utilisateur;
        $user->TELEPHONE = $phone;
        $user->ID_PAYS = $obj->ID;
        $user->save();

        //statut
        $result = array("status" => "success");

       return $result;
    }//fin creation profil


}
