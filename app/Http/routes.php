<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/inscription', 'RestauController@afficheinscription');
Route::post('/inscription', 'AuthController@register');

Route::get('/logan', 'RestauController@afficheconnexion');
Route::post('/logan', 'AuthController@login');

Route::post('/storePlat', 'PlatController@store');
Route::get('/ajouterPlat', 'PlatController@Ajouter')->name('plat.ajouter');

Route::get('/planing', 'RestauController@events');

//Les routes pour le menu
Route::get('/menus', 'MenuController@Ajouter')->name('menu.ajouter');
Route::post('/enregistrer', 'MenuController@Store');

Route::get('/listeMenu', 'RestauController@checklistfood');
Route::get('/formulaire', 'RestauController@form');
Route::get('/listeClient', 'RestauController@checklistClient');
Route::get('/Ecommande' , 'RestauController@savecommande');
Route::get('/supprimer/{id_client}', 'RestauController@deleteclient');
Route::post('/rafraichirclient/{id_client}', 'RestauController@deleteclient');
Route::get('/modifierclient/{id_client}', 'RestauController@editClient');

Route::get('/', 'RestauController@acceuil');

Route::auth();
