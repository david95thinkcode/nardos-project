<?php

namespace App\Models\table;

use Illuminate\Database\Eloquent\Model;

class Membres extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'membres';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['pseudo', 'pass', 'email'];



}
