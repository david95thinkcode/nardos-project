<?php

namespace App\Models\table;

use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'restaurant';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'tel', 'email' , 'adresse' , 'description'];



}
