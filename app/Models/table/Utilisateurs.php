<?php

namespace App\Models\table;

use Illuminate\Database\Eloquent\Model;

class Utilisateurs extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'utilisateurs';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_utilisateurs';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'id','pseudo', 'pass', 'email'];



}
