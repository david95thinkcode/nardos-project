<?php

namespace App\Models\tableRestau;

use Illuminate\Database\Eloquent\Model;

class MenuPlat extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menuplat';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_menuplat';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id_plat', 'id_menu'];



}
