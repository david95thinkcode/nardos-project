<?php

namespace App\Models\tableRestau;

use Illuminate\Database\Eloquent\Model;

class Plat extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'plats';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id_plat';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nom', 'image', 'description'];



}
