<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Utilisateur extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'UTILISATEUR';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['TELEPHONE', 'AUTRES', 'ID_PAYS'];

     //l pays correspondant
     public function pays() {
        return $this->belongsTo('App\Models\Pays', 'ID_PAYS', 'ID');
    }



}
