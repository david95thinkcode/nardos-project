<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Periodicite extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'PERIODICITE';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ID';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['ORDRE', 'HEURE' ];



}
