@extends('layouts/admin')
@section('title')Ajouter un menu
@endsection

@section('content')
<section class="content">
	<div class="box-body">
		<form action="{{ URL::to ('/enregistrer') }}" method="post">
			<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
			<div class="box-body">
				<div class="form-group">
					<label for="text" class="col-sm-2 control-label">Nom </label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="libmenu" placeholder="">

					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="text" class="col-sm-2 control-label"> liste des repas</label>
				<div class="col-sm-10">
					<select class="form-control" name="plats[]" multiple size='4'>

						@foreach($plats as $item)
						<option value="{{$item->id_plat}}"> {{$item->nom}} </option>
						@endforeach

					</select>
				</div>
			</div>

			<br>
			<br>
			<!-- /.col -->
			<div class="col-xs-4">
				<button type="submit" name="" class="btn btn-primary btn-block btn-flat">Creer</button>
			</div>
	</div>
	</form>
	</div>
	<section class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			@if (session('status'))
			<div class="alert alert-success text-center" role="alert">
				{{ session('status') }}
			</div>
			@endif
		</div>

	</section>
@endsection