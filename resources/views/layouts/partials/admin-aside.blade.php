<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{asset('../../dist/img/avatar.png')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Utilisateur</p>
          <a href="#"><i class="fa fa-circle text-success"></i> En ligne</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Gestion des Menus</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
               <li><a <a href="{{ route('plat.ajouter') }}"></i>Enregistrer plat</a></li>
            <li><a href="{{ route('menu.ajouter') }}"><i class="fa fa-circle-o"></i> Creer Menu</a></li>
             <li><a href="{{ URL::to ('/listeMenu')}}"><i class="fa fa-circle-o"></i> liste Menu</a></li>
          </ul>
        </li>
       
       
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Gestions Clients</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ URL::to ('/formulaire') }}"><i class="fa fa-circle-o"></i> Enregistrer Client</a></li>
            <li><a href="{{ URL::to ('/Ecommande') }}"><i class="fa fa-circle-o"></i> Enregistrer commande client</a></li>
            <li><a href="{{ URL::to ('/chekclist') }}"><i class="fa fa-circle-o"></i> Liste des clients</a></li>
          </ul>
        </li>
         <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Mise a jour du bot</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../forms/general.html"><i class="fa fa-circle-o"></i> status</a></li>
            <li><a href="../forms/advanced.html"><i class="fa fa-circle-o"></i> Revision</a></li>
            <li><a href="../forms/editors.html"><i class="fa fa-circle-o"></i> Contacter</a></li>
          </ul>
        </li>
       
        <li>
          <a href="{{ URL::to ('/planing') }}">
            <i class="fa fa-calendar"></i> <span>Calendrier</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>
        <li>
          <a href="../mailbox/mailbox.html">
            <i class="fa fa-envelope"></i> <span>Mail</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
          </a>
        </li>
       
       
        <li><a href="../../documentation/index.html"><i class="fa fa-book"></i> <span>Documentation</span></a></li>
        <li class="header">LABELS</li>
        <li><a href="#"><i class="fa fa-circle-o text-red"></i> <span>Publier</span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Compte<span></a></li>
        <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>A propos de nous</span></a></li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>