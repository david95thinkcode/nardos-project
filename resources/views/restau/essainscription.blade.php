<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Inscription</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../plugins/iCheck/square/blue.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>IT-RESTAU</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
       <img src="../dist/img/1.png" alt=""><br><br>
    <p class="login-box-msg">Inscrivez vous</p>

    <form role="form" method="POST" action="{{ url('/register') }}">
    <input type="hidden" name="_token" value="{{ csrf_token()}}">
     
       <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
        <input id="name" type="text" class="form-control"   name="name" placeholder="Entrer pseudo"value="{{ old('name') }}" >
         @if ($errors->has('name'))
         <span class="glyphicon glyphicon-envelope form-control-feedback"><strong>{{ $errors->first('name') }}</strong></span>
         @endif
      </div>

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}"  />
        @if ($errors->has('email'))
        <span class="glyphicon glyphicon-envelope form-control-feedback"><strong>{{ $errors->first('email') }}</strong></span>
       @endif
      </div>

      <div class="form-group has-feedback">
         <input type="Password"  class="form-control" id="pass" name="password" placeholder="Entrer mot de passe"/>
            @if ($errors->has('password'))
          <span class="glyphicon glyphicon-lock form-control-feedback"> <strong>{{ $errors->first('password') }}</strong></span>
            @endif
      </div>
      
       <div class="form-group has-feedback">
        <input type="password" class="form-control" id="password-confirm" name="password_confirmation" placeholder="Comfirmer mot de passe"/>
        @if ($errors->has('password_confirmation'))
          <span class="glyphicon glyphicon-lock form-control-feedback"> <strong>{{ $errors->first('password_confirmation') }}</strong></span>
            @endif
      </div>
      
     <div class="row">
        <div class="col-xs-8">
          
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Inscription</button>
        </div>

		 </form>
		
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
