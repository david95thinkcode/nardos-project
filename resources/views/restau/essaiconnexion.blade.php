<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Connexion</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index2.html"><b>IT_RETAU</b></a>
  </div>
  <div class="login-box-body">
       <img src="../dist/img/1.png" alt=""><br><br>
    <p class="login-box-msg">Connectez vous</p>

    <form role="form" method="POST" action="{{ url('/login') }}">
 <input type="hidden" name="_token" value="{{ csrf_token()}}">
    

      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <input  id="email" class="form-control" name="email"  value="{{ old('email') }}" placeholder="Email">
   @if ($errors->has('email'))
        <span class="glyphicon glyphicon-lock form-control-feedback">  <strong>{{ $errors->first('email') }}</strong></span>
      
          @endif
      </div>

      
      <div class="form-group has-feedback">
        <input id="password" type="password" name="password" class="form-control" placeholder="Password">
         @if ($errors->has('password'))
        <span class="glyphicon glyphicon-lock form-control-feedback"> <strong>{{ $errors->first('password') }}</strong></span>
          @endif
      </div>




      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="remember"> Souvenez vous de moi
            </label>
          </div>
        </div>
        
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Connexion</button>
        </div>
      </div>
    </form>
    <a  class="btn btn-link" href="{{ url('/password/reset') }}">Mot de passe oublier</a><br>
    <a href="register.html" class="text-center">S'inscrire</a>
  </div>
</div>

<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
