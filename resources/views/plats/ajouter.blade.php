@extends('layouts/admin')
@section('title')Ajouter plat
@endsection

@section('content')
<section class="content">
    <div class="box box-success">
       
   <div class="box box-success">
            
              <h3 class="box-title">Enregistrer un plat</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" method="post" action="storePlat" enctype="multipart/form-data">
              <div class="box-body">
                <div class="form-group">
                  <label for="text" class="col-sm-2 control-label">Nom </label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="nomPlat" placeholder="Nom du plat">
                  </div>
                </div>
               </div>
            <!-- /.box-header -->
            <div class="box-body pad">
              <label for="text" class="col-sm-2 control-label">Description </label>
                <div class="col-sm-10">
                  <textarea class="textarea" name="description" placeholder="Ecrire votre description" style="width: 90%; height: 50px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
            </div>

            <div class="form-group">
               <label for="text" class="col-sm-2 control-label">Photo </label>
               <input type="file" id="exampleInputFile" name="photo"/>
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            <div class="col-sm-10">    


          <!-- <a href="{{asset('../../../dist/img')}}"> <button type="button" class="btn bg-olive btn-flat margin">Telecharger</button></a>     -->

             <!-- <div class="form-group">
                  <label for="text"  class="col-sm-2 control-label">Photo</label>
                  <input type="file" name="photo">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
                </div> -->


            </div>
          </div>
       
              <!-- /.box-body -->
               <div class="col-sm-20">   
               <center> 
             <button type="submit"  class="btn bg-olive btn-flat margin">Ajouter le plat</button>
              </center>
            </div>
              <!-- /.box-footer -->
             
            </form>

          
          </div>
          
              <!-- /.box-comment -->
              <div class="box-comment">

  </section>
@endsection